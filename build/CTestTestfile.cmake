# CMake generated Testfile for 
# Source directory: /home/ros/group13_ws/src
# Build directory: /home/ros/group13_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("au_crustcrawler_base")
subdirs("dynamixel_motor/dynamixel_driver")
subdirs("dynamixel_motor/dynamixel_motor")
subdirs("dynamixel_motor/dynamixel_tutorials")
subdirs("dynamixel_motor/dynamixel_controllers")
subdirs("rosserial/rosserial")
subdirs("rosserial/rosserial_arduino")
subdirs("rosserial/rosserial_mbed")
subdirs("rosserial/rosserial_msgs")
subdirs("rosserial/rosserial_python")
subdirs("rosserial/rosserial_tivac")
subdirs("rosserial/rosserial_vex_cortex")
subdirs("rosserial/rosserial_vex_v5")
subdirs("rosserial/rosserial_xbee")
subdirs("rosserial/rosserial_client")
subdirs("dynamixel_motor/dynamixel_msgs")
subdirs("arm_control")
subdirs("timed_roslaunch")
subdirs("rosserial/rosserial_server")
subdirs("rosserial/rosserial_embeddedlinux")
subdirs("rosserial/rosserial_test")
subdirs("rosserial/rosserial_windows")
