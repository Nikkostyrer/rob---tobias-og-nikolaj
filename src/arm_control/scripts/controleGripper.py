#!/usr/bin/env python

import actionlib
from control_msgs.msg import FollowJointTrajectoryAction
from control_msgs.msg import FollowJointTrajectoryFeedback
from control_msgs.msg import FollowJointTrajectoryResult
from control_msgs.msg import FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectoryPoint
from trajectory_msgs.msg import JointTrajectory
from RosNode import RosNode


#rostopic pub -1 /gripper/command std_msgs/Float64 0.6



if __name__ == '__main__':
    gripperNode = RosNode(nodeName="gripper",publishTopic2="/gripper/command")
    gripperNode.initNode()
    gripperNode.publish(gripperNode.pub2, 0.0)


