#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from gotoCoordinate import gotoCoordinate
from RosNode import RosNode
import numpy as np
from dynamixel_msgs.msg import JointState

def callback(data):
    
    rospy.loginfo(data.is_moving)
    
 



if __name__ == '__main__':
    armNode = RosNode(nodeName="gripperState", subscribeTopic2="/gripper/state", callback2=callback)
    armNode.initNode()
    armNode.subcribeAndWait("/gripper/state")
