import rospy
from std_msgs.msg import String
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState

class RosNode:
    
    def __init__(self, nodeName="none", subscribeTopic1="none", callback1="none", subscribeTopic2="none", callback2="none", subscribeTopic3 = "none", callback3 = "none", publishTopic1="none", publishTopic2="none"):
        
        self.NodeName = nodeName

        self.SubscribeTopic1 = subscribeTopic1
        self.Callback1 = callback1

        self.SubscribeTopic2 = subscribeTopic2
        self.Callback2 = callback2

        self.SubscribeTopic3 = subscribeTopic3
        self.Callback3 = callback3


        self.PublishTopic1 = publishTopic1
        self.PublishTopic2 = publishTopic2
   
        
        if(publishTopic1 != "none"):
            self.pub1 = rospy.Publisher(self.PublishTopic1, String, queue_size=10)
            self.pub1.lastVal = "none"
        if(publishTopic2 != "none"):
            self.pub2 = rospy.Publisher(self.PublishTopic2, Float64, queue_size=10)
            self.pub2.lastVal = 0
       

    def initNode(self):
        rospy.init_node(self.NodeName,anonymous=True )

    def subcribeAndWait(self, subscriber):
        print("Subscribing : " + subscriber)
        if(subscriber == self.SubscribeTopic1):
            self.sub1 = rospy.Subscriber(self.SubscribeTopic1, String, self.Callback1)
        elif(subscriber == self.SubscribeTopic2):
            self.sub2 = rospy.Subscriber(self.SubscribeTopic2, JointState, self.Callback2)
        elif(subscriber == self.SubscribeTopic3):
            self.sub3 = rospy.Subscriber(self.SubscribeTopic3, String, self.Callback3)
        
        rospy.spin()

    def subscribe(self, subscriber):
        if(subscriber == self.SubscribeTopic1):
            self.sub1 = rospy.Subscriber(self.SubscribeTopic1, String, self.Callback1)
        elif(subscriber == self.SubscribeTopic2):
            self.sub2 = rospy.Subscriber(self.SubscribeTopic2, JointState, self.Callback2)
        elif(subscriber == self.SubscribeTopic3):
            self.sub3 = rospy.Subscriber(self.SubscribeTopic3, String, self.Callback3)

    def wait(self):
        rospy.spin()


    def unregisterAllSubscribers(self):
        try:
            self.sub1.unregister()
        except NameError:
            print("no sub1")

        try:
            self.sub2.unregister()
        except NameError:
            print("no sub2")

        try:
            self.sub3.unregister()
        except NameError:
            print("no sub3")        


    def publish(self,publisher,data):
        print("publishing : " + publisher.name)
        publisher.publish(data)
        publisher.lastVal = data