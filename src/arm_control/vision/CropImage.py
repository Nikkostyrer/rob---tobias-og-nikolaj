#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 14:22:26 2019

@author: ros
"""

import cv2

def CropImage(image=0,outputFilename='',W=640,H=288,writeToDisk=False):
    #Default størrelse fra IP-kamera er 640x480 pixels.
    #Vi går ud fra at vi kun er interesseret i de øverste 288 pixels, da
    #robotten fylder ca. den nederste tredjedel.
    #Hvis man ikke angiver andre parametre, beskæres billedet automatisk til
    #denne størrelse.

    #image = cv2.imread(filename)
    image = image[0:H, 0:W, :]

    if writeToDisk == True:
        cv2.imwrite(outputFilename,image)

    return image