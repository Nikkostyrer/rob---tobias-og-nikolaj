from CropImage import CropImage
from ShapeDetection import FindBoxes
from TextDetection import TextDetection
from getImageFromStream import getImageFromStream

def updateCubeList(CubeList, pixels_pr_cm, showFinalResult = False, debug = False):
    #Cropping image from camera to ignore bottom third of image where robot
    #would confuse the image detection
    img_unclean_cropped = CropImage(getImageFromStream())
    #Finds the cubes in the image, updates CubeList with coordinates of them
    CubeList = FindBoxes(CubeList, img_unclean_cropped,
                     pixels_pr_cm, debug=debug)
    #Detects character in every cube.
    CubeList = TextDetection(CubeList, img_unclean_cropped, showFinalResult = showFinalResult, debug=debug)

    return CubeList