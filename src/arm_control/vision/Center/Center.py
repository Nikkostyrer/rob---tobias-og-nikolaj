#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
#print(cv2.__version__) #4.1.2
import numpy as np
#print(np.__version__) #1.17.4

def findCenter(img_gray): #tager et 640x480 billede i grayscale
    #Tag nedre halvdel af billede 480 pixels højt, række 240 til række 480
    #Tag midterste tredjedel af billede 640 pixels bredt, søjle 210 til søjle 420
    #(210 fra højre kant, 220 fra venstre kant fordi 640 ikke pænt kan deles med 3: 640/3 = 213.333...)
    crop_upper = 240
    crop_lower = 480
    crop_left  = 210
    crop_right = 420
    img_gray = img_gray[crop_upper:crop_lower,crop_left:crop_right]

    n_lines = img_gray.shape[0] #find antal rækker i billedet
    n_columns = img_gray.shape[1] #find antal søjler i billedet

    #Initialiser threshold til 5 rækker, der befinder sig et stykke ovenfor
    #den øvre kant af kassen. Disse rækker vil blot være af den grå bordplade,
    #ledningen til motorene er muligvis med i rækkerne
    threshold_arr = [sum(img_gray[21]), sum(img_gray[22]), sum(img_gray[23]), sum(img_gray[24]), sum(img_gray[25])]
    threshold = 0.9 * sum(threshold_arr) / 5 #gns af de sidste 5 rækkers pixelværdier
    
    i = 0 #start ved række 0
    while i <= n_lines:
        #summér pixelværdier i rækken
        luminosity = sum(img_gray[i])
    
        #se om vi er under vores threshold
        if luminosity < threshold:
            upper_edge = i
            break
        i += 1 #tag næste række ned
    

    #Initialiser threshold til 5 søjler, der befinder sig et stykke til venstre
    #for den venstre kant af kassen. Disse rækker vil blot være af den grå
    #bordplade, muligvis med mørke skygger, robotten kaster. Det kommer an på
    #hvordan robotten står i forhold til lyset i loftet.
    threshold_arr = [sum(img_gray[:,31]), sum(img_gray[:,32]), sum(img_gray[:,33]), sum(img_gray[:,34]), sum(img_gray[:,35])]
    threshold = 0.9 * sum(threshold_arr) / 5 #gns af de sidste 5 søjlers pixelværdier
    
    i = 0 #start ved søjle 0
    while i <= n_columns:
        #summér pixelværdier i søjlen
        luminosity = sum(img_gray[:,i])
    
        #se om vi er under vores threshold
        if luminosity < threshold:
            left_edge = i
            break
        i += 1 #tag næste søjle til højre
    
    
    i = (n_columns -1) #start ved søjle 340 i billedet
    while i >= 0:
        #summér pixelværdier i søjlen
        luminosity = sum(img_gray[:,i])
    
        #se om vi er under vores threshold
        if luminosity < threshold:
            right_edge = i
            break
        i -= 1 #tag næste søjle til venstre


    #Man kan evt. opdatere threshold's værdier med de sidste 5 rækker/søjler:
#    arrayets plads 0, 1, 2, 3 = de gamle værdier på pladser 1, 2, 3, 4
#    threshold_arr[0:4] = threshold_arr[1:5]
#    opdater plads 4 i arrayet med pixelværdierne fra den nuværende række/søjle
#    threshold_arr[4] = luminosity

    
    robot_width_pixels = right_edge - left_edge
    x = round( robot_width_pixels / 2 + crop_left + left_edge )
    y = round( upper_edge + crop_upper + (robot_width_pixels * 2/3) )

    #robots bredde i cm: 11.43x11.43 cm, iflg.
    #"AX-12A / AX18A Smart Robotic Arm Specifications"
    pixels_pr_cm = robot_width_pixels / 11.43
    cm_pr_pixels = 1/pixels_pr_cm

    return x, y, pixels_pr_cm, cm_pr_pixels


#filename = 'Robot.jpeg'
filename = 'image_2.jpg'
#filename = "http://192.168.0.20\image\jpeg.cgi"
img = cv2.imread(filename,0) #option 0 åbner billede som grayscale
x, y = findCenter(img)

#print(x)
#print(y)


