#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 11:08:52 2019

@author: ros
"""

import cv2
import numpy as np

def WhiteBalance(image_reference, image_imbalanced):
    image_reference = image_reference[260:310,420:570,:]
    image_imbalanced_crop = image_imbalanced[260:310,420:570,:]


    B_sum1 = sum(sum( np.int64(image_reference[:,:,0]) ))
    G_sum1 = sum(sum( np.int64(image_reference[:,:,1]) ))
    R_sum1 = sum(sum( np.int64(image_reference[:,:,2]) ))

    B_sum2 = sum(sum(np.int64(image_imbalanced_crop[:,:,0])))
    G_sum2 = sum(sum(np.int64(image_imbalanced_crop[:,:,1])))
    R_sum2 = sum(sum(np.int64(image_imbalanced_crop[:,:,2])))

    B_scale = B_sum1 / B_sum2
    G_scale = G_sum1 / G_sum2
    R_scale = R_sum1 / R_sum2

    image_balanced = image_imbalanced
    image_balanced[:,:,0] = np.uint8(np.clip((B_scale*image_imbalanced[:,:,0]),0,255))
    image_balanced[:,:,1] = np.uint8(np.clip((G_scale*image_imbalanced[:,:,1]),0,255))
    #image_balanced[:,:,1] = np.uint8(G_scale*image_imbalanced[:,:,1])
    #image_balanced[:,:,2] = np.uint8(R_scale*image_imbalanced[:,:,2])
    image_balanced[:,:,2] = np.uint8(np.clip((B_scale*image_imbalanced[:,:,2]),0,255))

    return (image_balanced)

image_reference = cv2.imread('./Clean.jpeg')
image_imbalanced = cv2.imread('./Unclean2.jpeg')


image_balanced = WhiteBalance(image_reference, image_imbalanced)

cv2.imshow('Clean',image_reference)
if cv2.waitKey(0) & 0xFF == ord('q'):
    print("1")
cv2.destroyWindow('Clean')
cv2.imshow('Imbalanced',image_imbalanced)
if cv2.waitKey(0) & 0xFF == ord('q'):
    print("1")
cv2.destroyWindow('Imbalanced')
cv2.imshow('Balanced',image_balanced)
if cv2.waitKey(0) & 0xFF == ord('q'):
    print("2")
cv2.destroyWindow('Balanced')