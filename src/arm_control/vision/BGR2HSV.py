#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 14:42:21 2019

@author: ros
"""


import cv2

def BGR2HSV(imageBGR):
    imageHSV = cv2.cvtColor(imageBGR, cv2.COLOR_BGR2HSV)
    return imageHSV