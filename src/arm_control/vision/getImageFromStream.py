
import cv2

def getImageFromStream():
    stream_addr = 'http://192.168.0.20/video.cgi'
    #stream_addr = 'admin@http://192.168.0.20/video.cgi?.mjpg'
    #stream_addr = '192.168.0.20/mjpeg.cgi'
    img_addr = 'http://192.168.0.20/image/jpeg.cgi'
    cap = cv2.VideoCapture(stream_addr)

    if(cap.isOpened()):
        # Read next frame
        ret, frame = cap.read()
        
        if not ret:
            return 0

        return frame

    else:
        print("cap is NOT open")
        return 0