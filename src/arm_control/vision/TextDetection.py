# import the necessary packages
import pytesseract
import cv2
import numpy as np
import os

class Letter:
# Constructor
    def __init__(self, filename, image):
        self.filename = filename
        self.image = image

letter_list = []

def TextDetection(CubeList, image, debug=False, showFinalResult=False):
    #Load alle billeder af bogstaver i folderen letters. De skal bruges til
    #at kryds-korrelere vores billeder senere
    file_list = os.listdir('./letters')
    for filename in file_list:
        filename = './letters/' + filename
        absPath = os.path.abspath(filename)
        letterImage = cv2.imread(absPath, 0)
        letter_list.append( Letter(filename, letterImage) )


    #results = []
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    for Cube in CubeList:
        #extract the actual padded ROI from the non-resized original image
        #roi = orig[startY:endY, startX:endX]
        startX = getattr(Cube, 'startX')
        startY = getattr(Cube, 'startY')
        endX   = getattr(Cube, 'endX')
        endY   = getattr(Cube, 'endY')
        #roi = image[startY:endY, startX:endX]
        roi = gray[startY:endY, startX:endX]

        #kryds-korreler klods med vores kendte klodser:
        #https://docs.opencv.org/master/d4/dc6/tutorial_py_template_matching.html
        #corr_threshold = 0.8
        bestMaxVal = 0;
        #bestMinVal = 99999999999
        bestLetter = '';
        for letter in letter_list:
            corr_matrix = cv2.matchTemplate(roi, letter.image, cv2.TM_CCORR_NORMED)
            #np.where(corr_matrix > corr_threshold)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(corr_matrix)
    # If the largest value is the success criteria:
            if(max_val > bestMaxVal and max_val > 0.95):
                bestMaxVal = max_val
                if(debug == True):
                    print(max_val)
                bestLetter = letter.filename[10]
    # If the smallest value is the success criteria:
            # if(min_val < bestMinVal):
            #     bestMinVal = min_val
            #     bestLetter = letter.filename
        Cube.char = bestLetter
        # [54] = First letter in the image's file name (54 first chars is the path)




        # #sharpen filter:
        # kernel = -np.ones((3,3)) * 1
        # kernel[1,1] = 30
        # kernel = 1/9 * kernel
        # roi = cv2.filter2D(roi, ddepth = cv2.CV_8U, kernel=kernel)

        # if debug == True:
        #     cv2.imshow('Roi_sharpened', roi)
        #     if cv2.waitKey(0) & 0xFF == ord('q'):
        #         print('Stopping')
        #     cv2.destroyWindow('Roi_sharpened')

        # #antag at text er engelsk, brug OCR Engine Mode (oem) 1 og
        # #Page Segmentation Mode 10 (antag single character)
        # #config = ("-l eng --oem 1 --psm 10")
        # config = ("-l eng --oem 1 --psm 10 -c tessedit_char_whitelist=0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        # text = pytesseract.image_to_string(roi, config=config)
        # pytesseract.Output

        # #results.append(((startX, startY, endX, endY), text))
        # setattr(Cube, 'char', text)

        if showFinalResult==True or debug==True:
            print(Cube.char)
            cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 1)
            cv2.putText(image, Cube.char, (startX, startY - 20), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 255), 3)

    if showFinalResult==True or debug==True:
        #for ((startX, startY, endX, endY), text) in results:
#        cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 2)
#        cv2.putText(image, text, (startX, startY - 20), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 255), 3)

        cv2.imshow('ImageWithText_debug', image)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            print('Stopping')
        cv2.destroyWindow('ImageWithText_debug')

    return CubeList