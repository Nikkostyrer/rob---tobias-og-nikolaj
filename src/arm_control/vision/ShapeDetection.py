#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 09:47:48 2019

@author: ros
"""


import numpy as np
import imutils
import cv2
from Cube import Cube
from BGR2HSV import BGR2HSV

def DiffImg(img_clean, img_unclean):
    #træk clean og unclean fra hinanden. Konverter til int for at
    #forhindre at uint8-værdier underflower
    diff = np.int_(img_clean) - np.int_(img_unclean)
    diff = abs(diff)

    # #loop gennem alle pixels, i for rækker, j for søjler, k for farvekanaler BGR
    # for i in range(len(img_clean)):
    #     for j in range(len(img_clean[i])):
    #         for k in range(img_clean.shape[2]):
    #             #negative tal sættes til 0, da resultat i sidste ende
    #             #skal være uint8
    #             if (diff[i,j,k] < 0):
    #                 diff[i,j,k] = 0

    #konverter datatypen tilbage til uint8
    img_diff  = np.uint8(diff)
    return img_diff


def FindBoxes(CubeList, image_unclean, pixels_pr_cm, debug=False):
    #image = DiffImg(image_clean, image_unclean)

    #konverter billede fra BGR til HSV (hue, saturation, value)
    img_HSV = BGR2HSV(image_unclean)

    #Se i HSV-spektret om saturation (kanal 1)
    #og og value (kanal 2) er større end vores thresholds
    #Farv de thresholdede værdier hvide (255),
    #de øvrige pixels sættes til sort (0)
    saturation_threshold = 120
    value_threshold = 120

    image = np.uint8((img_HSV[:,:,1] > saturation_threshold) *
                     (img_HSV[:,:,2] > value_threshold) * 255)

    if (debug == True):
        cv2.imshow('diff',image)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            print("Diff")
        cv2.destroyWindow('diff')


    # =========================================================================
    #
    #FORETAG EROSION AF DET THRESHOLDEDE BILLEDE
    #Skal det være før eller efter blur?
    #Skal det være før eller efter dilation?
    #Hvis vi bruger resize()-funktionen nedenfor til f.eks. at resize til
    #halv størrelse, vil vores småfejl, der fejlagtigt er kommet gennem
    #vores thresholding, også blive mindre og lettere at erodere
    #
    # =========================================================================

    #De næste linjer kode er taget fra
    #https://www.pyimagesearch.com/2016/02/08/opencv-shape-detection/
    # Resize it to a smaller factor so that
    # the shapes can be approximated better
    resized = imutils.resize(image, width=640)
    ratio = image.shape[0] / float(resized.shape[0])
    # Convert the resized image to grayscale, blur it slightly,
    # and threshold it
#    gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    gray = image
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    #thresh = cv2.threshold(blurred, 10, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.threshold(blurred, 10, 255, cv2.THRESH_BINARY)[1]

    if(debug == True):
        cv2.imshow('gray',gray)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            print("Thresholded")
        cv2.destroyWindow('gray')
    
        cv2.imshow('BlurThresh_binary',thresh)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            print("Thresholded")
        cv2.destroyWindow('BlurThresh_binary')

    #Forsøg at dilate vores thresholdede konturer, for at udfylde evt. pixels
    #inde i klodserne, der uheldigvis er blevet sat = 0
    #6 pixels høj, 4 pixels bred. Fundet eksperimentelt.
    kernel = np.ones([6,4],np.uint8)
    thresh_dilated = cv2.dilate(thresh, kernel)

    #De næste linjer kode er taget fra
    #https://www.pyimagesearch.com/2016/02/08/opencv-shape-detection/
    # find contours in the thresholded image and initialize the
    # shape detector
    cnts = cv2.findContours(thresh_dilated.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)


    #N_cnts: antal konturere fundet med findContours() og grab_contours()
    N_cnts = len(cnts)
    #Præ-alloker array til at gemme box-koordinater
    #(startX,startY,endX,endY), dvs. 4 pladser til hver kontur
    boxes = np.array(np.zeros([N_cnts,4],np.int))



    i = 0
    for c in cnts:
        #De næste linjer kode er taget fra
        #https://www.pyimagesearch.com/2016/02/08/opencv-shape-detection/
        # Compute the center of the contour, then detect the name of the
        # shape using only the contour
        M = cv2.moments(c)
        cX = int((M["m10"] / M["m00"]) * ratio)
        cY = int((M["m01"] / M["m00"]) * ratio)

        #Klodserne er ca. 3.1x3.1cm
        #Det er bedst hvis vores boxes rundt om teksten ikke går ud over
        #kanten på klodserne, så vi fjerner ca. 1 cm fra kassens sidelængde,
        #derfor ganget med 2.15 cm i stedet for 3.1 cm
        box_width_pixels = pixels_pr_cm * 2.15
        #box_width_pixels = 20
        startX = int(cX - box_width_pixels/2)
        endX   = int(cX + box_width_pixels/2)
        startY = int(cY - box_width_pixels/2)
        endY   = int(cY + box_width_pixels/2)

        if(startX < 0 or startY < 0):
            continue

        CubeList.append( Cube(startX,startY,endX,endY,cX,cY) )
        # setattr(CubeList[i], 'startX', startX)
        # setattr(CubeList[i], 'startY', startY)
        # setattr(CubeList[i], 'endX', endX)
        # setattr(CubeList[i], 'endY', endY)
        # setattr(CubeList[i], 'cX', cX)
        # setattr(CubeList[i], 'cY', cY)

        boxes[i] = (startX, startY, endX, endY)
        #boxes[i] = (startX, startY, endX, endY, cX, cY)
        i += 1

        if debug==True:
            #De næste linjer kode er taget fra
            #https://www.pyimagesearch.com/2016/02/08/opencv-shape-detection/
            # Multiply the contour (x, y)-coordinates by the resize ratio
            # then draw the contours and the name of the shape on the image
            c = c.astype("float")
            c *= ratio
            c = c.astype("int")
            cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
            #cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
            #	0.5, (255, 255, 255), 2)


    if debug==True:
        for b in range(0,len(boxes)):
            #tegn røde firkanter om alle detekterede klodser
            cv2.rectangle(image_unclean, (boxes[b,0], boxes[b,1]), (boxes[b,2], boxes[b,3]), (0, 0, 255), 1)
            #cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 2)
            #boxes[b][0:2]

        cv2.imshow('ShapeDetection_debug',image_unclean)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            print("FindBoxes()")
        cv2.destroyWindow('ShapeDetection_debug')



    return CubeList
    #return boxes

