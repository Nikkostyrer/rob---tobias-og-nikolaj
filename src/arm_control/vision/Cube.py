# Class that contains all the data found on each cube on the image.
# Objects of this class should all be kept in a single array that of all
# cubes currently in the image.

class Cube:
# Constructor
    def __init__(self, startX, startY, endX, endY, cX, cY):
        self.startX = startX
        self.startY = startY
        self.endX = endX
        self.endY = endY
        self.cX = cX
        self.cY = cY

# Coordinates for the cube
    startX = 0.0
    endX = 0.0
    startY = 0.0
    endY = 0.0
    cX = 0.0
    cY = 0.0

# The character(s) detected on the cube
    char = ''